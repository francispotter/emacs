# FP Emacs configuration

Personalized hacks for everyday emacs

Includes:

- Supports some MacOS modifier keys for some operations (if set up in iTerm)
- Removes silly features like the splash screen, tilde files, and menus
- Support for MacOS pastebin using M-c and M-v
- Hopefully slightly more intelligent tab indentation
- Markdown and YAML modes, copied and tweaked
- Key bindings more closely align with other MacOS/Windows apps (but using ctrl rather than command) such as ctrl-w to close, ctrl-s to save, etc
- Dark theme
- Default word wrapping

We override a lot of behaviour and key bindings here, so if you have muscle memory for traditional emacs keys, beware!


## Installation

On MacOS:

```
curl https://gitlab.com/francispotter/emacs/-/raw/master/install.sh | sh
```

